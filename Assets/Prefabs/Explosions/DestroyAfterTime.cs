﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

	public float deathTime;

	// Use this for initialization
	void Start () {
		StartCoroutine (GetCoroutine ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private IEnumerator GetCoroutine(){
		yield return new WaitForSeconds (deathTime);
		Destroy (gameObject);
	}
}
