﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public static GameController GC;
	public PlayerController player;
	public GameObject explosion_1;
	public Scenario scenario;
	public GameObject endingText;
	public GameObject darkness;
	public string loseScene;
	public string winScene;
	public bool lightsStartOn;

	private bool gameOver = false;

	//These two lists should be a Map, but I'm too lazy to look up the generic C# library version
	public List<Faction> factions; 
	public List<List<GameObject>> factionUnits; //parallel list to factions

	void Awake () {
		GC = this;
		factions = new List<Faction> ();
		factionUnits = new List<List<GameObject>>();
	}

	// Use this for initialization
	void Start () {
		scenario = new Scenario.Deathmatch ();
	}
	
	// Update is called once per frame
	void Update () {
		if (gameOver) {
			return;
		}
		if (scenario != null) {
			if(scenario.CheckCompletion()){
				string scene = null;
				if(scenario.CheckPlayerWin()){
					endingText.GetComponent<UnityEngine.UI.Text>().text = "VICTORY";
					scene = winScene;
				}else{
					endingText.GetComponent<UnityEngine.UI.Text>().text = "DEFEAT";
					scene = loseScene;
				}
				darkness.GetComponent<UnityEngine.UI.Image>().color = darkness.GetComponent<UnityEngine.UI.Image>().color + new Color(0, 0, 0, 0.4f);
				gameOver = true;
				StartCoroutine(EndAfterDelay(5f, scene));
			}
		}
	}

	public IEnumerator EndAfterDelay(float delay, string scene){
		yield return new WaitForSeconds (delay);
		Application.LoadLevel (scene);
	}

	public void RegisterFactionUnit(GameObject go, Faction f){
		bool added = false;
		for(int i=0;i<factions.Count;i++){
			factionUnits[i].Remove(go);
			if(factions[i] == f){
				factionUnits[i].Add(go);
				added = true;
			}
		}
		if (!added) {
			factions.Add(f);
			List<GameObject> newList = new List<GameObject>();
			factionUnits.Add (newList);
			newList.Add (go);
		}
	}

	public bool CheckIfFactionIsDead(Faction f){
		for(int i=0;i<factions.Count;i++){
			if(factions[i] == f){
				foreach(GameObject go in factionUnits[i]){
					if(go.GetComponent<UnitController>().countsForTeamLive && !go.GetComponent<UnitController>().isdead){
						return false;
					}
				}
				return true;
			}
		}
		return true;
	}
}
