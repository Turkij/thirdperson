﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlyOverNav : NavigationInterface {

	protected override void Update ()
	{
		Vector3 dist;
		if (waypoint == null) {//even if we have no waypoint, still handle height changing
			dist = Time.deltaTime * GetComponent<FlyingUnit> ().GetHeightReccomendation (Vector3.zero);
			dist *= GetComponent<UnitController> ().speed;
			transform.position += dist;
		} else {
			dist = Time.deltaTime * GetComponent<FlyingUnit> ().GetHeightReccomendation (waypoint.transform.position - transform.position) * GetComponent<UnitController> ().speed;
			if (Vector3.Distance (transform.position, waypoint.transform.position) < dist.magnitude) {
				transform.position = waypoint.transform.position;
			} else {
				transform.position += dist;
			}
		}
	}

	/*
	Vector3 oldWaypoint;
	List<Vector3> path;
	public float avoidHeight;
	public const float recalcDistance = 10;

	public override GameObject waypoint{
		set{
			base.waypoint = value;
			UpdatePath();
		}
	}

	protected override void Start (){
		base.Start ();
		path = new List<Vector3> ();
	}

	protected override void Update ()
	{
		if (waypoint == null)
			path.Clear ();
			return;
		
		if (path.Count > 1){
			if(Vector3.Distance(oldWaypoint, waypoint.transform.position) > recalcDistance) {
				UpdatePath();
				oldWaypoint = waypoint.transform.position;
			}
		}else{
			path[0] = waypoint.transform.position;
		}
		if (base.StepTowards (path [0])) {
			path.RemoveAt(0);
		}
	}

	protected virtual void UpdatePath(){
		path.Clear ();
		path.Add (transform.position);
		path.Add (waypoint.transform.position);
		UpdatePathRecursive (0);
	}

	private void UpdatePathRecursive(int firstIndex){

		//print (path.Count);
		RaycastHit hit;
		print (path[firstIndex] + ", " + path[firstIndex+1]);
		print(Physics.Linecast (path[firstIndex], path[firstIndex+1], out hit));
		if (hit.collider != null) {//we hit something, add a new point to go over it
			Vector3 newPoint = Vector3.Lerp(path[firstIndex], path[firstIndex+1], 0.5f);
			RaycastHit hit2;
			if(Physics.Raycast(newPoint, Vector3.up, out hit2, Mathf.Infinity, LayerMask.NameToLayer("Terrain"))){
				newPoint = hit2.point + new Vector3(0, avoidHeight, 0);
			}

			path.Insert(firstIndex+1, newPoint);
			print (path.Count);
			int oldlen = path.Count;//we need this in case the first call makes the list longer
			UpdatePathRecursive (firstIndex);
			UpdatePathRecursive (firstIndex+1+(path.Count - oldlen));
		}

	}
	*/
}
