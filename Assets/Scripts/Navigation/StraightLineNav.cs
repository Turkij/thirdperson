﻿using UnityEngine;
using System.Collections;

public class StraightLineNav : NavigationInterface {
	protected override void Update ()
	{
		base.Update ();
		if (waypoint != null)
			StepTowards (waypoint.transform.position);
	}

	protected virtual bool StepTowards(Vector3 point){
		Vector3 dist = Time.deltaTime * (point - transform.position).normalized * GetComponent<UnitController> ().speed;
		if (Vector3.Distance (transform.position, point) < dist.magnitude) {
			transform.position = point;
			return true;
		} else {
			transform.position += dist;
			return false;
		}
	}
}
