﻿using UnityEngine;
using System.Collections;

public abstract class NavigationInterface : MonoBehaviour {

	private GameObject _waypoint;
	public virtual GameObject waypoint{
		set{
			_waypoint = value;
		}
		get{
			return _waypoint;
		}
	}

	// Use this for initialization
	protected virtual void Start () {

	}
	
	// Update is called once per frame
	protected virtual void Update () {
		
	}

	protected virtual void FixedUpdate () {
		
	}

	protected virtual void OnEnable(){

	}

	protected virtual void OnDisable(){
		
	}
}
