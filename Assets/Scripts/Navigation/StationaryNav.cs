﻿using UnityEngine;
using System.Collections;

public class StationaryNav : NavigationInterface {
	//I could have just made NavigationInterface not be abstract and used that
	//but I like abstract classes
	//here's a house
	//      _______
	//    /        \
	//   /__________\
	//  |  _      _  |
	//  | |_|    |_| |
	//  |     _      |
	//  |    | |     |
	//  |    | |     |
}
