﻿using UnityEngine;
using System.Collections;

public class NavAgentNav : NavigationInterface {
	
	public override GameObject waypoint{
		set{
			base.waypoint = value;
			NavMeshAgent navAgent = GetComponent<NavMeshAgent> ();
			if (waypoint == null) {
				navAgent.enabled = false;
			} else {
				navAgent.enabled = true;
				navAgent.destination = waypoint.transform.position;
				navAgent.enabled = true;
			}
		}
	}
	
	protected override void FixedUpdate(){
		NavMeshAgent navAgent = GetComponent<NavMeshAgent> ();
		UnitController controller = GetComponent<UnitController> ();
		navAgent.speed = controller.speed;
		navAgent.angularSpeed = controller.rotationSpeed;
	}

	protected override void OnEnable(){
		GetComponent<NavMeshAgent> ().enabled = true;
		if(GetComponent<Rigidbody>() != null)
			GetComponent<Rigidbody>().isKinematic = true;
	}
	
	protected override void OnDisable(){
		GetComponent<NavMeshAgent> ().enabled = false;
		if(GetComponent<Rigidbody>() != null)
			GetComponent<Rigidbody>().isKinematic = false;
	}
	
}
