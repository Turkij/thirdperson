﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AIAttitude {

	public static AIAttitude STANDARD = new Standard();
	public static AIAttitude AGGRESSIVE = new Aggressive();

	public abstract void HandleBehavior(UnitController controller);
	public bool Prob(int prob){//out of 100
		return Random.value * 100 < prob;
	}

	private class Standard : AIAttitude{
		private const float ATTACK_RANGE = 50f;

		public override void HandleBehavior(UnitController controller){
			List<GameObject> targets = controller.GetEnemyUnitsByRange ();
			if (targets.Count > 0 && Vector3.Distance (targets [0].transform.position, controller.transform.position) < ATTACK_RANGE) {
				controller.AttemptAttack(targets[0]);
			}
		}
	}

	private class Aggressive : AIAttitude{
		private const float ATTACK_RANGE = 50f;
		private const float APPROACH_MIN_RANGE = 20f;
		//private bool circleRight;

		public Aggressive(){
			//circleRight = Prob (50);
		}
		
		public override void HandleBehavior(UnitController controller){
			List<GameObject> targets = controller.GetEnemyUnitsByRange ();
			if (targets.Count > 0) {
				controller.AttemptAttack (targets [0]);
				if (Vector3.Distance (targets [0].transform.position, controller.transform.position) > APPROACH_MIN_RANGE) {
					controller.navInterface.waypoint = targets [0];
					controller.navInterface.enabled = true;
				} else {
					controller.navInterface.enabled = false;
				}/* else {
					controller.waypoint = (targets[0].transform.position - controller.transform.position)
					controller.navAgent.enabled = true;
				}*/
			} else {
				controller.navInterface.enabled = false;
			}
		}
	}
}
