﻿using UnityEngine;
using System.Collections;

public class HelicopterController : TankController, FlyingUnit{
	
	public float distDownMin;
	public float distDownMax;
	public float spherecastRadius;

	public override void HandlePlayerControl(PlayerController controller){
		if(Input.GetKeyDown(KeyCode.C)){
			controller.SetFollowTarget(null);
		}
		if (isdead) {
			controller.transform.position = GetViewCameraPosition();
			controller.transform.LookAt(turret.transform.position);
			controller.transform.Rotate(new Vector3(camera_y_rotation_mod, 0, 0));
			return;
		}

		float x_axis = Input.GetAxis("Horizontal");
		float z_axis = Input.GetAxis("Vertical");
		//float y_axis = Input.GetAxis("Altitude"); //use this if you want the player to be able to control their elevetion
		Vector3 cumulativeInput = z_axis * transform.forward + x_axis * transform.right;// + y_axis * transform.up;
		cumulativeInput = GetHeightReccomendation (cumulativeInput);
		transform.position += cumulativeInput * Time.deltaTime * speed;

		barrelHolder.transform.rotation = cameraHolder.transform.rotation;

		if(!controller.lerping){
			float yRot = Input.GetAxis("Mouse Y")*2f;
			float xRot = Input.GetAxis("Mouse X")*2f;
			
			turret.transform.Rotate(new Vector3(0, xRot, 0));
			cameraHolder.transform.RotateAround(turret.transform.position, turret.transform.right, -yRot);
			controller.transform.position = GetViewCameraPosition();
			controller.transform.LookAt(turret.transform.position);
			controller.transform.Rotate(new Vector3(camera_y_rotation_mod, 0, 0));
		}

		if (Input.GetMouseButton (0) && !controller.lerping) {
			Shoot();
		}
		if(Input.GetKeyDown(KeyCode.L)){
			headlight.SetActive(!headlight.activeSelf);
		}
	}

	protected override void OnCollisionEnter(Collision col){
		if (!isdead &&  col.gameObject.layer == LayerMask.NameToLayer("Terrain")) {
			Kill ();
		}
	}

	public override void Kill(){
		isdead = true;
		Instantiate (GameController.GC.explosion_1, transform.position, Quaternion.identity);
		GetComponent<Rigidbody> ().isKinematic = false;
		GetComponent<Rigidbody> ().useGravity = true;
		GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.None;
		StartCoroutine (DieAfterDelay ());
	}

	private IEnumerator DieAfterDelay(){
		yield return new WaitForSeconds (3);
		Instantiate (GameController.GC.explosion_1, transform.position, Quaternion.identity);
		/*if (GameController.GC.player.followTarget == gameObject) {
			GameController.GC.player.SetFollowTarget(null);
		}*/
		//GetComponent<Rigidbody> ().isKinematic = true;
		base.Kill ();
	}

	public virtual Vector3 GetHeightReccomendation(Vector3 intendedDir){//returns a normalized vector that it reccommends
		//Note: Figure out how to do one big hemisphere cast instead of two spherecasts
		RaycastHit hit;
		//if something is in front of us, go up
		if (intendedDir != Vector3.zero && Physics.SphereCast (transform.position, spherecastRadius, intendedDir, out hit, distDownMax, 1 << LayerMask.NameToLayer ("Terrain"))) {
			//we are going to run into something, go straight up!
			return Vector3.up;
		} else {
			if (Physics.SphereCast (transform.position, spherecastRadius, Vector3.down, out hit, distDownMax, 1 << LayerMask.NameToLayer ("Terrain"))) {
				if(Vector3.Distance(transform.position, hit.point) < distDownMin){
					//we are too low, go up
					if(intendedDir == Vector3.zero){
						return Vector3.up;
					}else{
						return (intendedDir + new Vector3(0, intendedDir.magnitude, 0)).normalized;
					}
				}else{
					//we are in the sweet spot
					return intendedDir.normalized;
				}
			}else{
				//we are too high, go down
				if(intendedDir == Vector3.zero){
					return Vector3.down;
				}else{
					return (intendedDir + new Vector3(0, -intendedDir.magnitude, 0)).normalized;
				}
			}
		}
	}

}
