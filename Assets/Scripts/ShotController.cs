﻿using UnityEngine;
using System.Collections;

public class ShotController : MonoBehaviour {

	private Faction _faction;

	public int damage;
	public float deathTime;
	private bool dead = false;

	public Faction faction{
		set{
			_faction = value;
			foreach(Renderer r in GetComponentsInChildren<Renderer>()){
				r.material = faction.material;
			}
		}
		get{
			return _faction;
		}
	}

	void OnCollisionEnter(Collision col){
		if (dead)
			return;
		if(col.gameObject.GetComponentInParent<ShotController>() != null && (faction == null || faction.IsEnemy (col.gameObject.GetComponentInParent<ShotController> ().faction))){
			col.gameObject.GetComponentInParent<ShotController>().Explode();
			Explode();
			dead = true;
		}
		if (col.gameObject.GetComponentInParent<UnitController> () != null && (faction == null || faction.IsEnemy (col.gameObject.GetComponentInParent<UnitController> ().faction))) {
			col.gameObject.GetComponentInParent<UnitController> ().health -= damage;
			Explode();
			dead = true;
			//hacky way of making shots not push units back
			if(col.gameObject.GetComponentInParent<Rigidbody>() != null && !col.gameObject.GetComponent<Rigidbody>().isKinematic){
				col.gameObject.GetComponentInParent<Rigidbody>().isKinematic = true;
				col.gameObject.GetComponentInParent<Rigidbody>().isKinematic = false;
			}
		}
	}
	// Use this for initialization
	void Start () {
		StartCoroutine (ExplodeAfterTime ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Explode(){
		Instantiate (GameController.GC.explosion_1, transform.position, Quaternion.identity);
		Destroy (gameObject);
	}

	private IEnumerator ExplodeAfterTime(){
		yield return new WaitForSeconds (deathTime);
		Explode ();
	}
}
