﻿using UnityEngine;
using System.Collections;

public interface FlyingUnit {
	Vector3 GetHeightReccomendation(Vector3 intendedDir);
}
