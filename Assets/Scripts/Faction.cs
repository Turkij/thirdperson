﻿using UnityEngine;
using System.Collections;

public abstract class Faction {

	public enum FactionEnum {NEUTRAL, BLUE, RED};//Wish C# had Java enums

	public static Faction GetFactionByEnum(FactionEnum fe){
		switch (fe) {
		case FactionEnum.NEUTRAL:
			return NEUTRAL;
		case FactionEnum.BLUE:
			return BLUE;
		case FactionEnum.RED:
			return RED;
		}
		throw new UnityException ("Unrecognised FactionEnum: " + fe);
	}
	
	//All factions are Singletons
	public static Faction BLUE = new Blue();
	public static Faction RED = new Red();
	public static Faction NEUTRAL = new Neutral();

	public Material material;
	public virtual bool IsEnemy(Faction f){
		return f != NEUTRAL && f != this;
	}
	public virtual bool IsAlly(Faction f){
		return f == this;
	}

	private class Neutral : Faction{
		public Neutral(){
			material = (Material)Resources.Load("Materials/Neutral");
		}
		public override bool IsEnemy(Faction f){
			return false;
		}
	}

	private class Blue : Faction{
		public Blue(){
			material = (Material)Resources.Load("Materials/Blue");
		}

	}

	private class Red : Faction{
		public Red(){
			material = (Material)Resources.Load("Materials/Red");
		}

	}
}
