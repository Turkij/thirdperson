﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class UnitController : MonoBehaviour {
		
	private Faction _faction;
	private bool _ai_on = true;
	public bool isdead;
	public int maxHealth;
	public int _health;
	public float speed;
	public float rotationSpeed;
	public GameObject targetPoint;
	public GameObject headlight;
	public bool countsForTeamLive;

	public NavigationInterface navInterface;
	public AIAttitude attitude;
	public Faction.FactionEnum factionEnum;

	public Faction faction{
		set{
			_faction = value;
			GameController.GC.RegisterFactionUnit(gameObject, _faction);
			UpdateColor();
		}
		get{
			return _faction;
		}
	}

	public int health{
		set{
			_health = value;
			if(health <= 0 && !isdead){
				Kill();
			}
			UpdateColor();
		}
		get{
			return _health;
		}
	}

	public virtual void UpdateColor(){
		Color c;
		if (isdead) {
			c = Color.gray;
		} else if (faction == null) {
			c = Color.Lerp(Color.white, Color.gray, (1 - (maxHealth == 0 ? 1 : ((float)health / maxHealth))) * 0.8f);
		} else {
			c = Color.Lerp(faction.material.color, Color.gray, (1 - (maxHealth == 0 ? 1 : ((float)health / maxHealth))) * 0.8f);
		}
		foreach (Renderer r in GetComponentsInChildren<Renderer>()) {
			r.material.color = c;
		}
	}

	public bool ai_on{
		set{
			_ai_on = value;
			if(_ai_on){
				navInterface.enabled = !isdead;
			}else{
				navInterface.enabled = false;
			}
		}
		get{
			return _ai_on;
		}
	}

	// Use this for initialization
	protected virtual void Start () {
		navInterface = GetComponent<NavigationInterface> ();
		faction = Faction.GetFactionByEnum (factionEnum);
		attitude = AIAttitude.AGGRESSIVE;
		ai_on = true;
		health = maxHealth;
		UpdateColor ();
		if (GameController.GC.lightsStartOn) {
			headlight.SetActive(true);
		} else {
			headlight.SetActive(false);
		}
	}
	
	// Update is called once per frame
	protected virtual void Update (){
		if (!isdead && ai_on && attitude != null) {
			attitude.HandleBehavior(this);
		}
		if (!isdead && transform.position.y < -10) {
			Kill ();
		}
	}

	protected virtual void FixedUpdate (){

	}

	public List<GameObject> GetUnitsByRange(){
		List<GameObject> result = new List<GameObject> (GameObject.FindGameObjectsWithTag("Unit"));
		result.Sort (CompareGameObjectsByDistance);
		return result;
	}

	public List<GameObject> GetEnemyUnitsByRange(){
		List<GameObject> result = new List<GameObject> (GameObject.FindGameObjectsWithTag("Unit"));
		int i = 0;
		while(i < result.Count){
			if(faction != null && (!faction.IsEnemy(result[i].GetComponent<UnitController>().faction) || result[i].GetComponent<UnitController>().isdead)){
				result.RemoveAt(i);
			}else{
				i++;
			}
		}
		result.Sort (CompareGameObjectsByDistance);
		return result;
	}

	public abstract void AttemptAttack(GameObject target);
	public abstract void HandlePlayerControl (PlayerController controller);
	
	private int CompareGameObjectsByDistance(GameObject g1, GameObject g2){
		float d1 = Vector3.Distance (transform.position, g1.transform.position);
		float d2 = Vector3.Distance (transform.position, g2.transform.position);
		if (d1 > d2) {
			return 1;
		} else if (d1 < d2) {
			return -1;
		} else {
			return 0;
		}
	}

	public virtual void Kill(){
		navInterface.enabled = false;
		isdead = true;
		enabled = false;
	}

	public abstract Vector3 GetViewCameraPosition ();
	public abstract Quaternion GetViewCameraRotation ();

	protected virtual void OnCollisionEnter(Collision col){
		return;
	}

	protected virtual void OnEnable(){

	}

	protected virtual void OnDisable(){
		
	}
}
