﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Scenario {

	public abstract bool CheckCompletion();
	public abstract bool CheckPlayerWin();

	public class Deathmatch : Scenario{
		private Faction winningFaction;

		//true if either one or zero factions have any living units
		public override bool CheckCompletion(){
			winningFaction = null;
			foreach (Faction f in GameController.GC.factions) {
				if(!GameController.GC.CheckIfFactionIsDead(f)){
					if(winningFaction == null){//we haven't found a living team yet
						winningFaction = f;
					}else{//we already found a living team
						winningFaction = null;
						return false;
					}
				}
			}
			return true;
		}

		public override bool CheckPlayerWin(){
			return GameController.GC.player.faction == winningFaction;
		}

	}
}
