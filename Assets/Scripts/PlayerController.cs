﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float command_view_height = 100f;
	public float command_view_pan_speed = 1f;
	public float command_view_zoom_speed = 500f;
	public Faction faction;

	public GameObject followTarget = null;
	public bool lerping = false;
	public UnityStandardAssets.Characters.FirstPerson.MouseLook m_MouseLook;

	// Use this for initialization
	void Start () {
		GameController.GC.player = this;
		m_MouseLook = new UnityStandardAssets.Characters.FirstPerson.MouseLook ();
		faction = Faction.BLUE;
		Cursor.lockState = CursorLockMode.None;
	}
	
	// Update is called once per frame
	void Update () {
		if (followTarget == null) {
			float x_axis = Input.GetAxis("Horizontal");
			float y_axis = Input.GetAxis("Vertical");
			float scroll = -Input.GetAxis("Mouse ScrollWheel");
			if(Input.GetKey(KeyCode.PageDown)){
				scroll = -.1f;
			}
			if(Input.GetKey(KeyCode.PageUp)){
				scroll = .1f;
			}
			transform.position += new Vector3(command_view_pan_speed*x_axis, command_view_zoom_speed*scroll, command_view_pan_speed*y_axis);
			transform.position = new Vector3(transform.position.x, Mathf.Clamp (transform.position.y, 1, 900), transform.position.z);
			command_view_height = transform.position.y;
			if (Input.GetMouseButtonDown (0)) {
				Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				Physics.Raycast(ray, out hit, Mathf.Infinity);
				if(hit.collider){
					if(hit.collider.gameObject.GetComponentInParent<UnitController>() != null && faction != null && faction.IsAlly(hit.collider.gameObject.GetComponentInParent<UnitController>().faction) ){
						SetFollowTarget(hit.collider.gameObject.GetComponentInParent<UnitController>().gameObject);
					}
				}
			}
		}else{
			followTarget.GetComponent<UnitController>().HandlePlayerControl(this);
		}

	}
	
	public void SetFollowTarget(GameObject target){
		//reset the guy we were controlling
		if (followTarget != null) {
			transform.parent = null;
			followTarget.GetComponent<UnitController> ().ai_on = true;
			if(!followTarget.GetComponent<Rigidbody>().isKinematic){
				followTarget.GetComponent<Rigidbody>().velocity = Vector3.zero;
			}
		}
		//if there is no new guy, go to command mode
		if (target == null) {
			StartCoroutine (LerpToPos (new Vector3 (transform.position.x, command_view_height, transform.position.z), Quaternion.Euler(90, 0, 0), 30));
			followTarget = null;
			Cursor.lockState = CursorLockMode.None;
		//otherwise, go to the new guy
		} else {
			StartCoroutine (LerpToTarget (target, 30));
			followTarget = target;
			target.GetComponent<UnitController>().ai_on = false;
			Cursor.lockState = CursorLockMode.Locked;
		}
	}
	
	private IEnumerator LerpToTarget(GameObject target, int frames){
		lerping = true;
		Vector3 initialPos = transform.position;
		Quaternion initialRotation = transform.rotation;
		for(int i=0;i<frames;i++){
			if(target == null){
				break;
			}
			float t = (float)i/frames;
			transform.position = Vector3.Lerp(initialPos, target.GetComponent<UnitController>().GetViewCameraPosition() , t);
			transform.rotation = Quaternion.Lerp (initialRotation, target.GetComponent<UnitController>().GetViewCameraRotation(), t);
			yield return null;
		}
		if (target != null) {
			transform.rotation = target.GetComponent<UnitController> ().GetViewCameraRotation ();
			transform.position = target.GetComponent<UnitController> ().GetViewCameraPosition ();
			lerping = false;
		}
	}

	private IEnumerator LerpToPos(Vector3 target, Quaternion rotation, int frames){
		lerping = true;
		Vector3 initialPos = transform.position;
		Quaternion initialRotation = transform.rotation;
		for(int i=0;i<frames;i++){
			float t = (float)i/frames;
			transform.position = Vector3.Lerp(initialPos, target, t);
			transform.rotation = Quaternion.Lerp(initialRotation, rotation, t);
			yield return null;
		}
		transform.position = target;
		transform.rotation = rotation;
		lerping = false;
	}
}
