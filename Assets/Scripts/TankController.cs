﻿using UnityEngine;
using System.Collections;
public class TankController : UnitController {

	public int shotCooldown;

	public bool debug_print;

	public GameObject turret;
	public GameObject barrelHolder;
	public GameObject cameraHolder;
	public bool canShoot = true;
	public GameObject shot;
	public GameObject[] shotSpawns;
	public int shotSpawnSelected;
	public float shotspeed = 100;
	public float camera_y_rotation_mod;

	protected override void Start(){
		base.Start ();
		attitude = AIAttitude.AGGRESSIVE;
		StartCoroutine(ShotCooldown());
		shotSpawnSelected = 0;
	}

	public override void AttemptAttack (GameObject target) {
		Vector3 faceTarget = target.GetComponent<UnitController>().targetPoint.transform.position;
		
		turret.transform.LookAt (faceTarget);
		float xrot = turret.transform.rotation.eulerAngles.x;
		float yrot = turret.transform.rotation.eulerAngles.y;
		//float zrot = turret.transform.rotation.eulerAngles.z;
		turret.transform.eulerAngles = new Vector3 (0, yrot, 0);
		barrelHolder.transform.localEulerAngles = new Vector3 (xrot-2, 0, 0);
		RaycastHit hit;
		Physics.Linecast (shotSpawns[shotSpawnSelected].transform.position, target.transform.position, out hit);
		//linecast so we don't shoot at walls and stuff
		if (hit.collider != null && hit.collider.gameObject.GetComponentInParent<UnitController>() == target.GetComponentInParent<UnitController>()) {
			Shoot ();
		}
	}

	public void Shoot(){
		if(canShoot){
			GetComponent<AudioSource>().Play();
			GameObject go = (GameObject)Instantiate(shot, shotSpawns[shotSpawnSelected].transform.position, shotSpawns[shotSpawnSelected].transform.rotation);
			go.GetComponent<Rigidbody>().velocity = shotSpawns[shotSpawnSelected].transform.forward * shotspeed;
			shotSpawnSelected = (shotSpawnSelected+1)%shotSpawns.Length;
			go.GetComponent<ShotController>().faction = faction;
			StartCoroutine(ShotCooldown());
		}
	}

	public IEnumerator ShotCooldown(){
		canShoot = false;
		yield return new WaitForSeconds(shotCooldown);
		canShoot = true;
	}

	public override void HandlePlayerControl(PlayerController controller){
		if(Input.GetKeyDown(KeyCode.C)){
			controller.SetFollowTarget(null);
		}
		if (isdead) {
			controller.transform.position = GetViewCameraPosition();
			controller.transform.LookAt(turret.transform.position);
			controller.transform.Rotate(new Vector3(camera_y_rotation_mod, 0, 0));
			return;
		}

		float x_axis = Input.GetAxis("Horizontal");
		float y_axis = Input.GetAxis("Vertical");
		transform.position += Time.deltaTime * y_axis * transform.forward * speed;
		transform.Rotate(new Vector3(0, Time.deltaTime * x_axis*rotationSpeed, 0));
		turret.transform.Rotate(new Vector3(0, -Time.deltaTime * x_axis*rotationSpeed, 0));
		barrelHolder.transform.rotation = cameraHolder.transform.rotation;
		if (Input.GetMouseButton (0) && !controller.lerping) {
			Shoot();
		}

		if(Input.GetKeyDown(KeyCode.L)){
			headlight.SetActive(!headlight.activeSelf);
		}

		if(!controller.lerping){
			float yRot = Input.GetAxis("Mouse Y")*2f;
			float xRot = Input.GetAxis("Mouse X")*2f;
			
			turret.transform.Rotate(new Vector3(0, xRot, 0));
			cameraHolder.transform.RotateAround(turret.transform.position, turret.transform.right, -yRot);
			controller.transform.position = GetViewCameraPosition();
			controller.transform.LookAt(turret.transform.position);
			controller.transform.Rotate(new Vector3(camera_y_rotation_mod, 0, 0));
		}
	}

	public override Vector3 GetViewCameraPosition(){
		return cameraHolder.transform.position;
	}
	public override Quaternion GetViewCameraRotation(){
		return cameraHolder.transform.rotation;
	}

}
